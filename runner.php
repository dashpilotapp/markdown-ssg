<?php

require_once("vendor/autoload.php");

// frontYAML
$parser = new Mni\FrontYAML\Parser();

// Twig
$loader = new Twig_Loader_Filesystem('_template');
$twig = new Twig_Environment($loader, array(
    'debug' => TRUE,
));


mkdir('public');
copy('_template/style.css', 'public/style.css');

$path = '_content/posts/';
$config_path = '_content/config/';

$data = array();
$data['site'] = array("title"=>"My Static Website");

// pages

if(file_exists($config_path.'pages.json')){
    
    $data['pages'] = json_decode(file_get_contents($config_path.'pages.json'));
}else{
    
    $pages = listFiles($path);
    
    $i=0;
    foreach($pages as $page){
        
        if(is_dir($path.$page)){
            
        $page = cleanPage($page);
            
        $title = ucwords($page);
        if($page=='home' || $page=='index'){
            $url = 'index.html';
        }else{
            $url = $page.'.html';
        }
            
        $data['pages'][$i] = array("title"=>$title, "url"=>$url);
        $i++;
        }
    }
    
}

// posts

if(file_exists($path)){

    $pages = listFiles($path);

    
    foreach($pages as $page){
        
        if(is_dir($path.$page)){
            
            $posts = listFiles($path.$page);
            
            $i=0;
            foreach($posts as $post){
                
                $ext = pathinfo($post, PATHINFO_EXTENSION);
                
                if($ext=='md'){

                $raw = file_get_contents($path.$page."/".$post);
                $doc = $parser->parse($raw);

                $data['posts'][$i] = $doc->getYAML();
                $data['posts'][$i]['body'] = $doc->getContent();
                
                $i++;
                }
          
            }
            
            $template = $twig->load('index.html');
            $templ = $template->render($data);
            
            $page = cleanPage($page);
            
            if($page=='home'){ $page='index'; }
            file_put_contents('public/'.$page.'.html', $templ);
            
            unset($data['posts']);
            
            
        }
     
    }
  
  
}

// functions
function cleanPage($page){
    
    $page = strtolower($page);
    
    if(strpos($page, '.')==1){
        $page = explode('.', $page);
        $page = $page[1];
    }           
    return $page;
}

function listFiles($dir){
    $items = array_filter(scandir($dir), function ($item) {
    return 0 !== strpos($item, '.');
    });
    
    return $items;
}


?>