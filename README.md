# Markdown Twig Static Site Generator for GitLab CI (php)

Simple blog systems that converts folders of markdown files to pages. It uses the Twig templating engine.

The conversion is done by running php in GitLab's CI

## How does it work?

Add Markdown files in the **_content/posts/1.home** folder, and they will automatically be converted into a site. You can also ass YAML frontmatter to use in your templates.

### Categories/pages

Making different categories/pages is simple: just create subfolders in the **_content/posts** directory, and put your posts in those subfolders. Each subfolder will be a seperate page on your site, and will also appear in the menu. Change the order of pages in the menu by prepending a number and a dot to the folder name, e.g **2.about**. Those numbers will not appear in the menu or the url of the page name.

### Drafts

To create a draft for a page or a post, just add a dot to the beginning of the filename. Once you're ready to publish, remove the dot.

### Templates

Templates use the Twig templating language. Currently available objects are:

- site: contains general website settings, title, etc.
- pages: contains the menu
- posts: contains the posts for the current page. The markdown is contained within post.body, and you can easily add your own data through YAML frontmatter.



